FROM python:3.8
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

ADD . /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000
RUN python /usr/src/app/script.py