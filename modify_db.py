#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2
from datetime import datetime
from uuid import uuid4
import psycopg2.extras

con = psycopg2.connect(database='gitlab-api', user='kuknos',
                       password='s$cret')

conn = psycopg2.connect(
   database="postgres", user='postgres', password='postgres' , host='127.0.0.1', port= '5432')

conn.autocommit = True


def add_project(name,gitlab_id):
    with con:

        cur = con.cursor()

        cur.execute("DROP TABLE IF EXISTS projects")
        cur.execute("CREATE TABLE projects(id SERIAL PRIMARY KEY, name VARCHAR(255) , gitlab_id INT)")
        cur.execute("INSERT INTO projects(name,gitlab_id) VALUES('{}',{})".format(name,gitlab_id))
    
    with conn:

        cursor = conn.cursor()
        cursor.execute("DROP DATABASE IF EXISTS {}".format(name))
        cursor.execute("CREATE DATABASE {}".format(name))


'''

def register(username, password, email):
    with conn:

        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("INSERT INTO users (name, password, email) VALUES (%s, %s, %s)",(username, password, email))



def login(username, password):

    with conn :
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT id from users WHERE users.name = %s AND users.password = %s", (username,password))
        rec = cur.fetchone()
        if rec is None:
            return None
        user_id = rec["id"]
        token = str(uuid4())
        exp_date = datetime.now()
        cur.execute("INSERT INTO tokens (user_id, token, exp_date) VALUES (%s, %s, %s)",(user_id, token, exp_date))

    return token



def authenticate(token):
    """
    :return: the user id the given token is valid for, or None if the token is invalid
    """
    conn = psycopg2.connect("dbname=postgres")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute("SELECT user_id FROM tokens WHERE token = %s",
                (token,))
    rec = cur.fetchone()
    if rec is None:
        return None
    user_id = rec["user_id"]
    conn.commit()
    cur.close()
    conn.close()
    return user_id



    '''